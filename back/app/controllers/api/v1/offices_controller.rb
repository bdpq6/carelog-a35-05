class Api::V1::OfficesController < ApplicationController
  def index
    if params[:keyword]
      @offices = Office.page(params[:page]).per(30)
                .keyword(params[:keyword])
    elsif params[:area] && params[:prefecture] && params[:cities]
      @offices = Office.page(params[:page]).per(30)
                .area(params[:area])
                .prefecture(params[:prefecture])
                .cities(params[:cities])
    else
      @offices = Office.page(params[:page]).per(30)
    end
    render json: { 'offices': @offices }
  end

  def show
    @office = Office.find_by(id: params[:id])

    if current_api_v1_user
      # 閲覧履歴を保存するコード
      new_history = @office.histories.new(user_id: current_api_v1_user.id)
      # 同一記事の重複チェック・重複時は古い履歴を削除
      if current_api_v1_user.histories.exists?(office_id: "#{params[:id]}")
        old_history = current_api_v1_user.histories.find_by(office_id: "#{params[:id]}")
        old_history.destroy
      end
      new_history.save
    end

    render json: { 'office': @office }
  end

  def edit
    @office = Office.find_by(id: params[:id])
    render json: { 'office': @office }
  end

  def destroy
    @office = Office.find_by(id: params[:id])
    @office.destroy
    render json: {'office': @office}
  end

end
