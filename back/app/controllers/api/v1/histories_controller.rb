class Api::V1::HistoriesController < ApplicationController
  before_action :authenticate_api_v1_user!

  def index
    user = User.find_by(id: current_api_v1_user.id)
    histories = History.page(params[:page]).per(10)
                .where("user_id=?",current_api_v1_user.id)
                .order("created_at DESC")
    render json: { 'histories': histories }
  end
end
